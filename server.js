const express = require('express')
const app = express()
const bodyParser = require('body-parser')
require('dotenv').config()
const port = process.env.PORT || 3000
const verifyToken = require('./middlewares/verifyToken');

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

let routes = require('./routes/route') //importing route
routes(app)

app.use(verifyToken);

/* app.use(function(req, res) {
    res.status(404).send({url: req.originalUrl + ' not found'})
}) */

app.listen(port)

console.log('RESTful API server started on: ' + port)

/* Version 1.1.0 */
