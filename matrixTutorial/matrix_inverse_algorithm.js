math = require("mathjs")
// Tính ma trận nghịch đảo của ma trận M
function matrix_invert(M) {
  // Dùng phương pháp Gauss: ( C|I ) ----------------->  ( I | C^-1 )
  // Sử dụng 3 phép biến đổi sơ cấp
  // (a) Hoán đổi vị trí 2 hàng
  // (b) Nhân một hàng với một hệ số khác 0
  // (c) cộng một hàng nhân với một hệ số vào một hàng khác

  // Nếu ma trận không phải ma trận vuông -> return undefined
  if (M.length !== M[0].length) {
    return -1;
  }

  // Tạo ma trận đơn vị I và ma trận C từ ma trận đầu vào -> C | I
  var i = 0,
    below = 0,
    j = 0,
    dim = M.length,
    e = 0,
    t = 0;
  var I = [],
    C = [];
  for (i = 0; i < dim; i += 1) {
    // Tạo mới 2 ma trận có số row bằng với số row ma trận đầu vào
    I[I.length] = [];
    C[C.length] = [];
    for (j = 0; j < dim; j += 1) {      
      // Nếu nằm trên đường chéo, phần tử có giá trị = 1
      if (i == j) {
        I[i][j] = 1;
      // Nếu không nằm trên đường chéo, phần tử có giá trị = 0
      } else {
        I[i][j] = 0;
      }

      // Copy từng phần tử ma trận đầu vào M vào ma trận C
      C[i][j] = M[i][j];
    }
  }

  //Duyệt qua từng hàng của ma trận C
  for (i = 0; i < dim; i += 1) {
    // Lấy ra phần tử e nằm trên đường chéo
    e = C[i][i];
    // Nếu phần tử trên đường chéo bằng 0 => đổi chỗ hàng hiện tại với hàng ở dưới gần nhất có phần tử trên cột i khác 0
    if (e == 0) {
      // Duyệt qua từng hàng phía dưới
      for (below = i + 1; below < dim; below += 1) {
        // Nếu có phần tử trên cột i khác 0
        if (C[below][i] != 0) {          
          // Đổi chỗ 2 hàng với nhau
          for (j = 0; j < dim; j++) {
            // Đổi chỗ trên ma trận C
            e = C[i][j]; 
            C[i][j] = C[below][j]; 
            C[below][j] = e; 
            // ĐỔi chỗ trên ma trận đơn vị ban đầu I
            e = I[i][j]; 
            I[i][j] = I[below][j]; 
            I[below][j] = e; 
          }
          // Nếu đã tìm được hàng thỏa mãn và swap, không cần check tiếp các hàng tiếp theo
          break;
        }
      }
      // Kiểm tra phần tử e trên đường chéo chính sau khi swap
      e = C[i][i];
      // Nếu vẫn có giá trị 0 -> ma trận đầu vào M là không khả nghịch -> return undefined
      if (e == 0) {
        return;
      }
    }
    
    // Chia hàng thứ i bởi hệ số e - phần tử trên đường chéo chính -> để có phần tử 1 trên đường chéo chính
    for (j = 0; j < dim; j++) {
      // Áp dụng với ma trận đầu vào  
      C[i][j] = C[i][j] / e; 
      // Áp dụng với ma trận đơn vị I ban đầu
      I[i][j] = I[i][j] / e; 
    }

    // Với mỗi row khác với row đang xét hiện tại, lấy ra hệ số  e = phần tử trên cột j của row đó
    // Trừ row đó bằng giá trị row hiện tại * hệ số e
    // Mục đích để mỗi phần tử ở cột j (ngoài đường chéo chính) sẽ có giá trị bằng 0
    for (below = 0; below < dim; below++) {
      // Nếu là row đang xét -> bỏ qua
      if (below == i) {
        continue;
      }

      // Lấy ra hệ số e
      e = C[below][i];

      // Trừ row đó bằng giá trị row hiện tại * hệ số e      
      for (j = 0; j < dim; j++) {
        // Áp dụng cho ma trận copy đầu vào ban đầu C
        C[below][j] -= e * C[i][j]; 
        // Áp dụng cho ma trận đơn vị ban đầu I
        I[below][j] -= e * I[i][j]; 
      }
    }
  }

  //Sau khi hoàn thành các phép biến đổi, ma trận C sẽ trở về dạng đơn vị
  // Và ma trận I sẽ là ma trận nghịch đảo của ma trận đầu vào ban đầu M  
  // ( C | I ) ----------------> ( I | C ^ -1)  
  for (let i = 0; i < dim; i++) {
    for (let j = 0; j < dim; j++) {
      I[i][j] = math.round(I[i][j], 5);
    }
  }
  return I;
}

function printMatrix(matrix) {
  let rowText = "";
  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[0].length; j++) {
      rowText += matrix[i][j] + " ";
    }
    console.log(rowText);
    rowText = "";
  }
};

var inputMatrix = [];
inputMatrix.push(["5", "6"]);
inputMatrix.push(["7", "8"]);
var inverseMatrix = matrix_invert(inputMatrix);
if (typeof inverseMatrix !== "undefined") {
  printMatrix(inverseMatrix);
} else {
  console.log("Ma trận đầu vào không thể nghịch đảo");
}

module.exports = { printMatrix, matrix_invert};
