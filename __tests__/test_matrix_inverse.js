const function_tests = require("../matrixTutorial/matrix_inverse_algorithm");

describe("Test function Inverse Matrix", () => {
  test("This Testcase should calculate and return inverse matrix of the input matrix", () => {
    const inputMatrix = [["5", "6"], ["7", "8"]];
    const outputMatrix = function_tests.matrix_invert(inputMatrix);
    expect(outputMatrix).toEqual([[-4, 3], [7/2, -5/2]]);
  });

  test("This Testcase should calculate and return undefined", () => {
    const inputMatrix = [["0", "0"], ["7", "8"]];
    const outputMatrix = function_tests.matrix_invert(inputMatrix);
    expect(outputMatrix).toBeUndefined();
  });
});
