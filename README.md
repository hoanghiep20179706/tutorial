# REST API example application

Sample RESTful API with Nodejs

## Run the app

	npm run start

## Run the tests

	npm run test

# REST API

The REST API to the example app is described below.

## Login

### Request

	`POST /login/`

	curl --location --request POST 'http://103.107.183.254:3000/login' \
	--header 'auth-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYxNDEzMTI1MiwiZXhwIjoxNjE0MjE3NjUyfQ.KNR3GhOo9OLSiY0Q-D1xmdbT96oEo9DUpXjf1MvbF6Q' \
	--header 'Content-Type: application/json' \
	--data-raw '{
		"name": "Hoang Hiep",
		"email": "hoanghiep20179706@gmail.com",
		"password": "thaovtt1234"
	}'	
	
### Response (JWT)
	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYxNDEzMTYyMCwiZXhwIjoxNjE0MjE4MDIwfQ.XpENlAeekATlc9b7_-fYGOhhd8vchy2ALuTHv1y87PQ

## Get list of books

### Request

`GET /books`

curl --location --request GET 'http://103.107.183.254:3000/books' \
--header 'auth-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYxNDEzMjI0MSwiZXhwIjoxNjE0MjE4NjQxfQ.vYWoco-TWvInJ8HaNcH9lMltZdLD24-5FWsrfO5z_Qs' \
--header 'Content-Type: application/json' \
--data-raw '{
	"name": "Hoang Hiep",
	"email": "hoanghiep20179706@gmail.com",
	"password": "thaovtt1234"
}'

### Response
[
	{
		"id": 20,
		"title": "The Hobbit",
		"primary_author": "Tolkien"
	}
]

## Create a new Book

### Request

`POST /books`

curl --location --request POST 'http://103.107.183.254:3000/books' \
--header 'auth-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYxNDEzMjI0MSwiZXhwIjoxNjE0MjE4NjQxfQ.vYWoco-TWvInJ8HaNcH9lMltZdLD24-5FWsrfO5z_Qs' \
--header 'Content-Type: application/json' \
--data-raw '{
	"title": "The Hobbit",
	"primary_author": "Tolkien"
}'

### Response
[
	{
		"id": 20,
		"title": "The Hobbit",
		"primary_author": "Tolkien"
	}
]

## Get a specific book

### Request

`GET /books/:bookId`

curl --location --request GET 'http://103.107.183.254:3000/books/20' \
--header 'auth-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYxNDEzMjI0MSwiZXhwIjoxNjE0MjE4NjQxfQ.vYWoco-TWvInJ8HaNcH9lMltZdLD24-5FWsrfO5z_Qs' \
--header 'Content-Type: application/json' \
--data-raw '{
	"title": "The Hobbit",
	"primary_author": "Tolkien"
}'

### Response
[
	{
		"id": 20,
		"title": "The Hobbit",
		"primary_author": "Tolkien"
	}
]

## Update a book

### Request

`PUT /books/:bookId`

curl --location --request PUT 'http://103.107.183.254:3000/books/20' \
--header 'auth-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYxNDEzMjI0MSwiZXhwIjoxNjE0MjE4NjQxfQ.vYWoco-TWvInJ8HaNcH9lMltZdLD24-5FWsrfO5z_Qs' \
--header 'Content-Type: application/json' \
--data-raw '{
	"title": "The Hobbit",
	"primary_author": "Tolkien Update"
}'

### Response

Book modified with ID: 20