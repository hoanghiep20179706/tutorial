const jwt = require("jsonwebtoken");

module.exports = (request, response, next) => {
  console.log("verifying..");
  const token = request.header("accessToken");

  if (!token) return response.status(401).send("Access Denied");

  try {
    const verified = jwt.verify(token, process.env.TOKEN_SECRET);
    next();
  } catch (err) {
    return response.status(400).send("Invalid Token");
  }
};
