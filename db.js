const Pool = require("pg").Pool;
const pool = new Pool({
  user: "postgres",
  host: "103.107.183.254",
  database: "seta",
  password: "thaovtt1234",
  port: 8686,
});

const redis = require("redis");
const client = redis.createClient({
  host: "103.107.183.254",
  port: 9000,
});

client.on("connect", () => {
  console.log("Client connected to redis...");
});

client.on("ready", () => {
  console.log("Client connected to redis and ready to use...");
});

client.on("error", (err) => {
  console.log(err.message);
});

client.on("end", () => {
  console.log("Client disconnected from redis");
});

module.exports = { pool, client };
