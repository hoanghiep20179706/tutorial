"use strict";
const util = require("util");
const { pool } = require("../db");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { registerValidator } = require("../auth/validation");
const { json } = require("express");
const redis = require("redis");
const { client } = require("../db");
const createError = require("http-errors");
const {
  signAccessToken,
  signRefreshToken,
  verifyRefreshToken,
} = require("../auth/jwt");

module.exports = {
  register: async (request, response) => {
    const { name, email, password } = request.body;
    const { error } = registerValidator(request.body);

    if (error) return response.status(422).send(error.details[0].message);

    //const checkEmailExist = await findOne(email);

    //if (checkEmailExist) return response.status(422).send('Email is exist');

    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(password, salt);

    try {
      const insertPostgresPromise = new Promise(
        (resolve, reject) => {
          pool.query(
            "INSERT INTO users (name, email, password) VALUES ($1, $2, $3) RETURNING id",
            [name, email, hashPassword],
            (error, results) => {
              if (error) {
                response.status(500).send({
                  error,
                });
                return reject(error);
              }
            }
          );
        },
        (err) => {
          console.log("Errore insertPostgresPromise" + err);
        }
      );
      let newUserID = await insertPostgresPromise;
      const accessToken = await signAccessToken(newUserID);
      const refreshToken = await signRefreshToken(newUserID);
      response.status(201).send({
        accessToken,
        refreshToken,
        message: `User added with ID: ${newUserID}`,
      });
    } catch (error) {
      console.error(error);
      response.status(500).send({
        error,
      });
    }
  },

  login: async (request, response) => {
    console.log("login");
    const { email, password } = request.body;

    const results = await pool.query("SELECT * FROM users WHERE email = $1", [
      email,
    ]);

    const user = results.rows[0];
    console.log("user = " + JSON.stringify(user));

    if (!user) return response.status(422).send("Email is not correct");

    const checkPassword = await bcrypt.compare(password, user.password);

    if (!checkPassword)
      return response.status(422).send("Password is not correct");

    const accessToken = await signAccessToken(user.id);
    const refreshToken = await signRefreshToken(user.id);

    // Set browser httpOnly cookies
    response.cookie("access_token", accessToken, {
      httpOnly: true,
    });
    response.cookie("refresh_token", refreshToken, {
      httpOnly: true,
    });

    response
      .header("refreshToken", refreshToken)
      .header("accessToken", accessToken)
      .send({ accessToken, refreshToken });
  },

  refreshToken: async (req, res, next) => {
    try {
      const { refreshToken } = req.body;
      if (!refreshToken) throw createError.BadRequest();
      const userId = await verifyRefreshToken(refreshToken);

      const accessToken = await signAccessToken(userId);
      const refToken = await signRefreshToken(userId);
      res.send({ accessToken: accessToken, refreshToken: refToken });
    } catch (error) {
      next(error);
    }
  },

  logout: async (req, res, next) => {
    try {
      const { refreshToken } = req.body;
      if (!refreshToken) throw createError.BadRequest();
      const userId = await verifyRefreshToken(refreshToken);
      client.DEL(userId, (err, val) => {
        if (err) {
          console.log(err.message);
          throw createError.InternalServerError();
        }
        console.log(val);
        res.sendStatus(204);
      });
    } catch (error) {
      next(error);
    }
  },
};
