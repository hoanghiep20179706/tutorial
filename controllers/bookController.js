'use strict'
const util = require('util')
const { pool } = require('../db')

module.exports = {
    get: (request, response) => {
        pool.query('SELECT * FROM books ORDER BY id ASC', (error, results) => {
            if (error) {
                throw error
            }
            response.status(200).json(results.rows)
        })
    },
    detail: (request, response) => {
        const id = parseInt(request.params.bookId)
        pool.query('SELECT * FROM books WHERE id = $1', [id], (error, results) => {
            if (error) {
                throw error
            }
            response.status(200).json(results.rows)
        })
    },
    update: (request, response) => {
        const id = parseInt(request.params.bookId)
        const { title, primary_author } = request.body
        pool.query(
            'UPDATE books SET title = $1, primary_author = $2 WHERE id = $3',
            [title, primary_author, id],
            (error, results) => {
                if (error) {
                    throw error
                }
                response.status(200).send(`Book modified with ID: ${id}`)
            }
        )
    },
    create: (request, response) => {
        const { title, primary_author } = request.body
        pool.query('INSERT INTO books (title, primary_author) VALUES ($1, $2) RETURNING id', [title, primary_author], (error, results) => {
            if (error) {
                throw error
            }
            response.status(201).send(`Book added with ID: ${results.rows[0].id}`)
        })
    },
    delete: (request, response) => {
        const id = parseInt(request.params.bookId)
        pool.query('DELETE FROM books WHERE id = $1', [id], (error, results) => {
            if (error) {
                throw error
            }
            response.status(200).send(`Book deleted with ID: ${id}`)
        })
    },
}