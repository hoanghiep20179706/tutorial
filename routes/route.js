"use strict";

const verifyToken = require("../middlewares/verifyToken");

module.exports = function (app) {
  let bookController = require("../controllers/bookController");
  let userController = require("../controllers/userController");

  // todoList Routes
  /*app.route('/books')
        .get(bookController.get)
        .post(bookController.create); */
  app.get("/books", verifyToken, bookController.get);
  app.post("/books", verifyToken, bookController.get);

  app.get("/books/:bookId", verifyToken, bookController.detail);
  app.put("/books/:bookId", verifyToken, bookController.update);
  app.delete("/books/:bookId", verifyToken, bookController.delete);

  /* app.route('/books/:bookId')
        .get(bookController.detail)
        .put(bookController.update)
        .delete(bookController.delete); */
  app.route("/api/auth/register").post(userController.register);
  app.route("/api/auth").post(userController.login);
  app.route("/api/auth/refresh").post(userController.refreshToken);
  app.route("/api/auth/logout").post(userController.logout);
};
